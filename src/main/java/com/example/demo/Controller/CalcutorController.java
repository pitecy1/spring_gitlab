package com.example.demo.Controller;

import com.example.demo.Util.ResponseObj;
import com.example.demo.model.Calculator;
import org.springframework.web.bind.annotation.*;

@RestController
public class CalcutorController {


    @RequestMapping(path = "/calculator/cal" , method = RequestMethod.POST)
    public @ResponseBody ResponseObj mainController(@RequestBody Calculator calculator){
        double result = calculator.getA()+calculator.getB();
        return new ResponseObj(200,result);
    }

    @RequestMapping(path = "/calculator/caldata" , method = RequestMethod.POST)
    public @ResponseBody ResponseObj mainControllerdata(@RequestBody Calculator calculator){
        double result = calculator.getA()+calculator.getB();
        return new ResponseObj(200,result);
    }
    @RequestMapping(path = "/calculator/caldata1" , method = RequestMethod.POST)
    public @ResponseBody ResponseObj mainControllerdata1(@RequestBody Calculator calculator){
        double result = calculator.getA()+calculator.getB();
        return new ResponseObj(200,result);
    }

}
