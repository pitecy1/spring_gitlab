package com.example.demo.model;

import lombok.Getter;
import lombok.Setter;

public class Calculator {
    @Setter
    @Getter
    private String condition;
    @Setter
    @Getter
    private double a;
    @Setter
    @Getter
    private double b;
}
