package com.example.demo.Util;


import lombok.Getter;
import lombok.Setter;

public class ResponseObj {
    @Setter
    @Getter
    int code ;
    @Setter
    @Getter
    Object object;

    public ResponseObj(int code, Object object) {
        this.code = code;
        this.object = object;
    }
}
